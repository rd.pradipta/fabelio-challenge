# This file should responsible to manage database structure only
from django.db import models


class Product(models.Model):
    """
    Model for Product
    Defines the attributes of a product
    """
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=1024)
    price = models.IntegerField()
    dimension = models.CharField(max_length=1024)
    colour = models.CharField(max_length=1024)
    material = models.CharField(max_length=1024)
    image_link = models.CharField(max_length=1024)

    def __str__(self):
        return self.name
