# This file is responsible to manage CHALLENGE Administrator Panel only

from .models import Product
from django.contrib import admin

admin.site.site_header = "Challenge Administrator Panel"
admin.site.site_title = "Challenge Administrator Panel"


class ProductAdmin(admin.ModelAdmin):
    list_display = ["name", "price", "dimension", "colour", "material", "image_link"]
    list_filter = ["material"]


admin.site.register(Product, ProductAdmin)
