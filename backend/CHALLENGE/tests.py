# This file should responsible to manage unit & functional tests only
from django.apps import apps
from django.test import Client, TestCase
from django.urls import reverse
from rest_framework import status

from CHALLENGE.apps import ChallengeConfig
from CHALLENGE.models import Product
from CHALLENGE.serializers import ProductSerializer

client = Client()

OK = status.HTTP_200_OK
NOT_FOUND = status.HTTP_404_NOT_FOUND

PRODUCT_TEST = Product(
    id=1,
    name='Sofa 2 dudukan Vienna',
    price=3899000,
    dimension='162 x 95 x 86',
    colour='custard vienna, graphite vienna, ruby vienna',
    material='solid wood',
    image_link='https://fabelio.com/media/catalog/product/w/i/wina_2_seater_sofa__custard__1_1.jpg',
)

def save_all_basics():
    """
    Save all objects before starting unit tests
    This function will be called in setUp for each class of unit tests
    """
    PRODUCT_TEST.save()


class AppConfigUnitTest(TestCase):
    def test_apps(self):
        self.assertEqual(ChallengeConfig.name, "CHALLENGE")
        self.assertEqual(apps.get_app_config("CHALLENGE").name, "CHALLENGE")


class ProductUnitTest(TestCase):
    def setUp(self):
        save_all_basics()
        self.product = Product.objects.get(id=1)

    def test_product_object_found(self):
        self.assertIsNotNone(self.product)

    def test_get_all_product(self):
        response = client.get(reverse("product_list"))
        statuses = Product.objects.all()
        serializer = ProductSerializer(statuses, many=True)
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, OK)
