# This file should responsible to manage URL routings

from django.conf.urls import url
from .api import ProductList, ProductSimilar

urlpatterns = [
    url(r"^products/$", ProductList.as_view(), name="product_list"),
    url(r"^product/similar/(?P<id>[0-9A-Fa-f-]+)", ProductSimilar.as_view(), name="product_similar"),
]
