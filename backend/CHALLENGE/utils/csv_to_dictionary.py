# This file should responsible to manage conversion CSV to python dictionary

import csv, json

def convert_csv_to_dictionary(file_path):
    """
    Tools to convert .csv file into JSON
    Input: CSV file
    Output: Python dictionary file contains status, data or reason (if fails)

    Params:
    file_path : path of the file to be converted, should contain file name and file extension

    Example:
    file_path : home/input.csv

    Output:
    Fail: {
        'status' : 'fail',
        'reason' : 'File Not Found'
    }
    Success: {
        'status' : 'success',
        'data': [Array of JSON each row of the CSV file]
    }
    """
    return_data = {}
    try:
        file = open(file_path, "r", encoding="utf8")
        extension = file_path.split(".")[-1].lower()
        if extension != "csv":
            return_data["status"] = "fail"
            return_data["reason"] = "Not a CSV file"
        else:
            reader = csv.DictReader(file)
            return_data["data"] = []
            for rows in reader:
                try:
                    return_data["data"].append(rows)
                except UnicodeDecodeError:
                    # Skip rows with invalid encoding files
                    continue
            return_data["status"] = "success"
    except FileNotFoundError:
        return_data["status"] = "fail"
        return_data["reason"] = "File Not Found"
    except:
        return_data["status"] = "fail"
        return_data[
            "reason"
        ] = "Failed to process. You may uploaded wrong or bad csv file!"
    finally:
        # file.close()
        return return_data
