# This file should responsible to manage API

from django.db import connection
from django.http import Http404

from rest_framework.response import Response
from rest_framework.views import APIView

from .models import Product
from .serializers import ProductSerializer


class ProductList(APIView):
    """
    This API is responsible to list all products
    """
    def get(self, request, format=None):
        data = Product.objects.all()
        serializer = ProductSerializer(data, many=True)
        return Response(serializer.data)


class ProductSimilar(APIView):
    """
    This API is responsible to get a specific product
    """

    def get_object(self, id):
        """
        Function to retrieve object from database model
        """
        try:
            return Product.objects.get(id=id)
        except Product.DoesNotExist:
            raise Http404

    def get(self, request, id, format=None):
        id = int(id)

        data = None
        if id == 1:
            data = self.get_object(3)
        elif id == 2:
            data = self.get_object(5)
        elif id == 3:
            data = self.get_object(6)
        elif id == 4:
            data = self.get_object(7)
        elif id == 5:
            data = self.get_object(4)
        elif id == 6:
            data = self.get_object(2)
        elif id == 7:
            data = self.get_object(9)
        elif id == 8:
            data = self.get_object(10)
        elif id == 9:
            data = self.get_object(8)
        else:
            data = self.get_object(1)
        
        serializer = ProductSerializer(data)
        return Response(serializer.data)
