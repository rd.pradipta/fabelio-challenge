# # This file should responsible to manage other logic than API

# import os
# import sys
# import threading
# import datetime

# from CHALLENGE.models import Article, Assignment, Keyword, Rank
# from CHALLENGE.utils.csv_to_dictionary import convert_csv_to_dictionary
# from django.views.decorators.csrf import csrf_exempt
# from django.http import JsonResponse, HttpResponseNotAllowed, Http404, FileResponse
# from django.views import View
# from rest_framework import status
# from rest_framework.response import Response
# from rest_framework.views import APIView

# @csrf_exempt
# def file_download(request):
#     """
#     This function will download sample CSV files provided by system
#     """
#     if request.method == "GET":
#         file_path = os.path.join('static', 'sample_csv_files.zip')
#         response = FileResponse(open(file_path, 'rb'))
#         response['content_type'] = "application/octet-stream"
#         response['Content-Disposition'] = 'attachment; filename=' + os.path.basename(file_path)
#         return response
#     else:
#         return HttpResponseNotAllowed("POST")

# @csrf_exempt
# def file_import(request):
#     """
#     This function will handle POST request to receive CSV files from frontend
#     """
#     if request.method == "POST":
#         raw_file = request.FILES["file"]

#         today = datetime.date.today()
#         monday = today - datetime.timedelta(days=today.weekday())
#         sunday = monday + datetime.timedelta(days=6)
#         week = monday.strftime('%d %b') + ' - ' + sunday.strftime('%d %b')

#         file_data = save_to_temporary_storage_and_get_some_file_data(raw_file)
#         converted_csv = convert_csv_to_dictionary(file_data["file_path"])
#         os.remove(file_data["file_path"])

#         if converted_csv["status"] == "success":
#             background_process = None
#             found_path = execution_path_finder(converted_csv)
#             if found_path:
#                 if found_path == "ahrefs_rank":
#                     background_process = threading.Thread(
#                         target=execute_ahrefs_rank,
#                         args=(converted_csv, week))
#                 elif found_path == "ahrefs_pages":
#                     background_process = threading.Thread(
#                         target=execute_ahrefs_pages,
#                         args=(converted_csv, week))
#                 elif found_path == "ga_engagement":
#                     background_process = threading.Thread(
#                         target=execute_ga_engagement,
#                         args=(converted_csv, week))
#                 elif found_path == "gsc_pages":
#                     background_process = threading.Thread(
#                         target=execute_gsc_pages,
#                         args=(converted_csv, week))
#                 else:
#                     background_process = threading.Thread(
#                         target=execute_keywords,
#                         args=(converted_csv,))
#                 background_process.start()
#                 return JsonResponse({"status": "success"})
#             else:
#                 return JsonResponse(
#                     {
#                         "status": "fail",
#                         "reason": "Failed to process. You may uploaded wrong or bad csv file!",
#                     }
#                 )
#         else:
#             return JsonResponse(converted_csv)
#     return HttpResponseNotAllowed("GET")

# def save_to_temporary_storage_and_get_some_file_data(file):
#     """
#     This function will save the uploaded CSV for temporary use
#     This function works by writing chunks to temporary location
#     """
#     file_path = os.path.join("static", file.name)
#     file_directory = os.path.dirname(file_path)

#     if not os.path.exists(file_directory):
#         os.makedirs(file_directory)
#     with open(file_path, "wb+") as destination:
#         for chunk in file.chunks():
#             destination.write(chunk)
#     return { "file_path": file_path }

# def execution_path_finder(converted_csv):
#     """
#     This function will find the execution path of uploaded CSV file
#     since system currently only support upload + process 1 CSV at one time
#     This function will return string of the execution path
#     """
#     first_line = converted_csv["data"][0]
#     keys = first_line.keys()

#     try:
#         if ("Keyword" in keys) and \
#             ("URL" in keys):
            
#             # To Tackle Ms Excel CSV Creation Problem which only first header values availabe, otherwise None
#             if first_line["URL"]:
#                 return "ahrefs_rank"

#         elif ("URL Rating (desc)" in keys or "URL Rating" in keys) and \
#             ("Referring Domains" in keys) and \
#             ("Dofollow" in keys):
        
#             # To Tackle Ms Excel CSV Creation Problem which only first header values availabe, otherwise None
#             if first_line["Referring Domains"] and first_line["Dofollow"]:
#                 return "ahrefs_pages"
        
#         elif ("Bounce Rate" in keys) and \
#             ("Pages / Session" in keys) and \
#             ("Sess. Duration" in keys):
        
#             # To Tackle Ms Excel CSV Creation Problem which only first header values availabe, otherwise None
#             if first_line["Pages / Session"] and first_line["Sess. Duration"]:
#                 return "ga_engagement"
        
#         elif ("Clicks" in keys) and \
#             ("Impressions" in keys) and \
#             ("CTR" in keys) and \
#             ("Position" in keys):
            
#             # To Tackle Ms Excel CSV Creation Problem which only first header values availabe, otherwise None
#             if first_line["Impressions"] and first_line["CTR"] and first_line["Position"]:
#                 return "gsc_pages"
        
#         elif ("Keyword" in keys) and ("Volume" in keys):
            
#             # To Tackle Ms Excel CSV Creation Problem which only first header values availabe, otherwise None
#             if first_line["Volume"]:
#                 return "keywords"
        
#         return False
    
#     except KeyError:
#         return False

# def execute_ahrefs_rank(converted_csv, week):
#     """
#     This function will execute to add data by matching URL and Keyword
#     that already in database with information given by CSV file
    
#     First of all, it will filter all articles based on URL from CSV
#     If Article found, then iterate for each article to check whether URL and
#     Keyword from CSV are matched with database data

#     If matched, it will create or modify rank object to be stored in database
#     """
#     for element in converted_csv["data"]:
#         entry_keyword = element["Keyword"].lower()
#         entry_url = element["URL"].lower()
#         article_from_db = Article.objects.filter(publish_link=entry_url)
        
#         if article_from_db.count() != 0:
#             # Iterate for each article data, get corresponding url and keyword information
#             for article in article_from_db:
#                 url_from_db = article.publish_link
#                 keyword_from_db = article.assignment.keyword            
#                 # Start matching data from database and data from CSV
#                 if (keyword_from_db.keyword_content.lower() == entry_keyword) and \
#                     (url_from_db.lower() == entry_url):
#                     data = {
#                         "week": week,
#                         "rank_position": element["Position"]
#                     }
#                     try:
#                         # Article rank correspond already exists
#                         rank_from_db = Rank.objects.get(keyword=keyword_from_db.id)
#                         modify_rank(rank_from_db, data, "ahrefs_rank")
#                     except Rank.DoesNotExist:
#                         rank_to_create = create_initial_rank(keyword_from_db)
#                         modify_rank(rank_to_create, data, "ahrefs_rank")                    

# def execute_ahrefs_pages(converted_csv, week):
#     """
#     This function will execute to add data by matching Page URL
#     that already in database with information given by CSV file
    
#     First of all, it will filter all articles based on Page URL from CSV
#     If Article found, then iterate for each article to check whether
#     Page URL from CSV are matched with database data

#     If matched, it will create or modify rank object to be stored in database
#     """
#     for element in converted_csv["data"]:
#         entry_url = element["Page URL"].lower()

#         try:
#             entry_url_rating = element["URL Rating"]
#         except KeyError:
#             entry_url_rating = element["URL Rating (desc)"]

#         article_from_db = Article.objects.filter(publish_link=entry_url)

#         if article_from_db.count() != 0:
#             # Iterate for each article data, get corresponding url
#             for article in article_from_db:
#                 url_from_db = article.publish_link
#                 keyword_from_db = article.assignment.keyword
#                 # Start matching data from database and data from CSV
#                 if (url_from_db.lower() == entry_url):
#                     data = {
#                         "week": week,
#                         "url_rating": entry_url_rating,
#                         "ref_domain": element["Referring Domains"],
#                         "do_follow": element["Dofollow"]
#                     }
#                     try:
#                         rank_from_db = Rank.objects.get(keyword=keyword_from_db.id)
#                         # Article rank correspond already exists
#                         modify_rank(rank_from_db, data, "ahrefs_pages")
#                     except Rank.DoesNotExist:
#                         # Article rank correspond does not exist
#                         rank_to_create = create_initial_rank(keyword_from_db)
#                         modify_rank(rank_to_create, data, "ahrefs_pages")

# def execute_ga_engagement(converted_csv, week):
#     """
#     This function will execute to add data by matching Bounce Rate, Pages / Session,
#     and Sess Duration that already in database with information given from CSV file    
    
#     First of all, it will filter all articles based on Page URL from CSV
#     If Article found, then iterate for each article to check whether
#     Page URL from CSV are matched with database data

#     If matched, it will create or modify rank object to be stored in database
#     """
#     for element in converted_csv["data"]:
#         entry_url = element["Page"].lower()
#         article_from_db = Article.objects.filter(publish_link=entry_url)

#         if article_from_db.count() != 0:
#             # Iterate for each article data, get corresponding url
#             for article in article_from_db:
#                 url_from_db = article.publish_link
#                 keyword_from_db = article.assignment.keyword
#                 # Start matching data from database and data from CSV
#                 if (url_from_db.lower() == entry_url):
#                     data = {
#                         "week": week,
#                         "bounce_rate": element["Bounce Rate"],
#                         "pages_session": element["Pages / Session"],
#                         "sess_duration": element["Sess. Duration"]
#                     }
#                     try:
#                         rank_from_db = Rank.objects.get(keyword=keyword_from_db.id)
#                         # Article rank correspond already exists
#                         modify_rank(rank_from_db, data, "ga_engagement")
#                     except Rank.DoesNotExist:
#                         # Article rank correspond does not exist
#                         rank_to_create = create_initial_rank(keyword_from_db)
#                         modify_rank(rank_to_create, data, "ga_engagement")

# def execute_gsc_pages(converted_csv, week):
#     """
#     This function will execute to add data by matching Bounce Rate, Pages / Session,
#     and Sess Duration that already in database with information given from CSV file    
    
#     First of all, it will filter all articles based on Page URL from CSV
#     If Article found, then iterate for each article to check whether
#     Page URL from CSV are matched with database data

#     If matched, it will create or modify rank object to be stored in database
#     """
#     for element in converted_csv["data"]:
#         entry_url = element["Page"].lower()
#         article_from_db = Article.objects.filter(publish_link=entry_url)

#         if article_from_db.count() != 0:
#             # Iterate for each article data, get corresponding url
#             for article in article_from_db:
#                 url_from_db = article.publish_link
#                 keyword_from_db = article.assignment.keyword
#                 # Start matching data from database and data from CSV
#                 if (url_from_db.lower() == entry_url):
#                     data = {
#                         "week": week,
#                         "clicks": element["Clicks"],
#                         "impressions": element["Impressions"],
#                         "ctr": element["CTR"],
#                         "avg_position": element["Position"]
#                     }
#                     try:
#                         rank_from_db = Rank.objects.get(keyword=keyword_from_db.id)
#                         # Article rank correspond already exists
#                         modify_rank(rank_from_db, data, "gsc_pages")
#                     except Rank.DoesNotExist:
#                         # Article rank correspond does not exist
#                         rank_to_create = create_initial_rank(keyword_from_db)
#                         modify_rank(rank_to_create, data, "gsc_pages")

# def execute_keywords(converted_csv):
#     """
#     This function will execute to add Keywords and their corresponding volumes.
#     If Keyword already exists, update the volume based on CSV file
#     Else, create a new keyword object and volume in database
#     """
#     # Iterate trough converted csv data and find keyword from DB that matched with CSV
#     for element in converted_csv["data"]:
#         entry_keyword = element["Keyword"].lower()
#         entry_volume = element["Volume"]
#         try:
#             # If data found, update volume if there is a value change
#             keyword_from_db = Keyword.objects.get(keyword_content=entry_keyword)
#             if keyword_from_db.keyword_volume != int(entry_volume):
#                 data = keyword_from_db
#                 data.keyword_volume = entry_volume
#                 data.save()
#         except Keyword.DoesNotExist:
#             # If no keyword data found, create a new one
#             Keyword.objects.create(
#                 keyword_content=entry_keyword, keyword_volume=entry_volume
#             )

# def check_is_time_exists(week_to_check, array_of_dict):
#     """
#     Function to check whether, return False if keys does not exists && return index is exists
#     """
#     for i in range (len(array_of_dict)):
#         try:
#             if array_of_dict[i]['time'] == week_to_check:
#                 return i
#         except KeyError:
#             return False
#     return False

# def create_initial_rank(keyword_from_db):
#     """
#     Function to create initial rank object
#     """
#     rank_to_create = Rank(
#         keyword=keyword_from_db,
#         rank_position="[]",
#         url_rating="[]",
#         ref_domain="[]",
#         do_follow="[]",
#         bounce_rate="[]",
#         pages_session="[]",
#         sess_duration="[]",
#         clicks="[]",
#         impressions="[]",
#         ctr="[]",
#         avg_position="[]",
#     )
#     return rank_to_create

# def modify_rank(rank_object, data, execution_path):
#     """
#     This function will execute to add values to rank object based on CSV execution path
#     """
#     week = data["week"]
    
#     if execution_path == "ahrefs_rank":
#         data_rank_position = {}
#         rank_position = eval(rank_object.rank_position)

#         is_exists_index = check_is_time_exists(week, rank_position)

#         if type(is_exists_index) == bool :
#             data_rank_position["time"] = week
#             data_rank_position["value"] = int(data["rank_position"])
#             rank_position.append(data_rank_position)

#         else:
#             rank_position[is_exists_index]["value"] = int(data["rank_position"])
        
#         rank_object.rank_position = str(rank_position)
        
#     elif execution_path == "ahrefs_pages":
#         data_url_rating = {}
#         data_ref_domain = {}
#         data_do_follow = {}

#         url_rating = eval(rank_object.url_rating)
#         ref_domain = eval(rank_object.ref_domain)
#         do_follow = eval(rank_object.do_follow)
        
#         is_exists_index = check_is_time_exists(week, url_rating)

#         if type(is_exists_index) == bool :
#             data_url_rating["time"] = week
#             data_ref_domain["time"] = week
#             data_do_follow["time"] = week
#             data_url_rating["value"] = int(data["url_rating"])
#             data_ref_domain["value"] = int(data["ref_domain"])
#             data_do_follow["value"] = int(data["do_follow"])
#             url_rating.append(data_url_rating)
#             ref_domain.append(data_ref_domain)
#             do_follow.append(data_do_follow)

#         else:
#             url_rating[is_exists_index]["value"] = int(data["url_rating"])
#             ref_domain[is_exists_index]["value"] = int(data["ref_domain"])
#             do_follow[is_exists_index]["value"] = int(data["do_follow"])
        
#         rank_object.url_rating = str(url_rating)
#         rank_object.ref_domain = str(ref_domain)
#         rank_object.do_follow = str(do_follow) 

#     elif execution_path == "ga_engagement":
#         data_bounce_rate = {}
#         data_pages_session = {}
#         data_sess_duration = {}

#         bounce_rate = eval(rank_object.bounce_rate)
#         pages_session = eval(rank_object.pages_session)
#         sess_duration = eval(rank_object.sess_duration)

#         is_exists_index = check_is_time_exists(week, bounce_rate)    
#         bounce_rate_value = ('.').join(data["bounce_rate"][:-1].split(',')) # Take out percentage. 85,00% -> 85.0

#         if type(is_exists_index) == bool :
#             data_bounce_rate["time"] = week
#             data_pages_session["time"] = week
#             data_sess_duration["time"] = week
#             data_bounce_rate["value"] = float(bounce_rate_value)
#             data_pages_session["value"] = int(data["pages_session"])
#             data_sess_duration["value"] = int(data["sess_duration"])
#             bounce_rate.append(data_bounce_rate)
#             pages_session.append(data_pages_session)
#             sess_duration.append(data_sess_duration)
        
#         else:            
#             bounce_rate[is_exists_index]["value"] = float(bounce_rate_value)
#             pages_session[is_exists_index]["value"] = int(data["pages_session"])
#             sess_duration[is_exists_index]["value"] = int(data["sess_duration"])

#         rank_object.bounce_rate = str(bounce_rate)
#         rank_object.pages_session = str(pages_session)
#         rank_object.sess_duration = str(sess_duration) 
        
#     else:
#         data_clicks = {}
#         data_impressions = {}
#         data_ctr = {}
#         data_avg_position = {}

#         clicks = eval(rank_object.clicks)
#         impressions = eval(rank_object.impressions)
#         ctr = eval(rank_object.ctr)
#         avg_position = eval(rank_object.avg_position)

#         is_exists_index = check_is_time_exists(week, clicks)    
#         avg_position_value = ('.').join(data["avg_position"].split(',')) # Change "5,5" -> 5.5

#         if type(is_exists_index) == bool :
#             data_clicks["time"] = week
#             data_impressions["time"] = week
#             data_ctr["time"] = week
#             data_avg_position["time"] = week

#             data_clicks["value"] = int(data["clicks"])
#             data_impressions["value"] = int(data["impressions"])
#             data_ctr["value"] = float(data["ctr"][:-1])
#             data_avg_position["value"] = float(avg_position_value)

#             clicks.append(data_clicks)
#             impressions.append(data_impressions)
#             ctr.append(data_ctr)
#             avg_position.append(data_avg_position)

#         else:
#             clicks[is_exists_index]["value"] = int(data["clicks"])
#             impressions[is_exists_index]["value"] = int(data["impressions"])
#             ctr[is_exists_index]["value"] = float(data["ctr"][:-1])
#             avg_position[is_exists_index]["value"] = float(avg_position_value)

#         rank_object.clicks = str(clicks)
#         rank_object.impressions = str(impressions)
#         rank_object.ctr = str(ctr)
#         rank_object.avg_position = str(avg_position) 
        
#     return rank_object.save()
