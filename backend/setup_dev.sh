#!/bin/bash
# Please run this script for the first time running for development
export DJANGO_SETTINGS_MODULE=settings.dev
python manage.py makemigrations CHALLENGE
python manage.py migrate CHALLENGE
python manage.py migrate
python manage.py loaddata initial_data.json
