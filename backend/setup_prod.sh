#!/bin/bash
# This script will be run when deploying to Heroku
export DJANGO_SETTINGS_MODULE=settings.prod
python manage.py migrate CHALLENGE
python manage.py migrate
python manage.py loaddata initial_data.json
