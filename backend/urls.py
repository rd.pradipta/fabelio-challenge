from django.contrib import admin
from django.urls import path, include

from CHALLENGE import urls

urlpatterns = [
    path("__admin__/", admin.site.urls),
    path("v1/", include("CHALLENGE.urls")),
]
