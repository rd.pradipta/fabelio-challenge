"""Use this for production"""

import dj_database_url, os
from .base import *
from dotenv import load_dotenv

load_dotenv('.env.prod')

URL = os.getenv("BACKEND_URL")

DEBUG = True
ALLOWED_HOSTS += [URL]
WSGI_APPLICATION = "wsgi.prod.application"

DATABASES = {}
DATABASES["default"] = dj_database_url.config(conn_max_age=600)

AUTH_PASSWORD_VALIDATORS = [
    {
        "NAME": "django.contrib.auth.password_validation.UserAttributeSimilarityValidator"
    },
    {"NAME": "django.contrib.auth.password_validation.MinimumLengthValidator"},
    {"NAME": "django.contrib.auth.password_validation.CommonPasswordValidator"},
    {"NAME": "django.contrib.auth.password_validation.NumericPasswordValidator"},
]

STATICFILES_STORAGE = "whitenoise.storage.CompressedManifestStaticFilesStorage"

CORS_ORIGIN_ALLOW_ALL = True