from CHALLENGE.serializers import UserSerializer


def modify_jwt_login_response(token, user=None, request=None):
    """
    JWT login response modifier.
    When user logged in, instead of returning token only,
    this will return token + user information
    """
    return {
        "token": token,
        "user": UserSerializer(user, context={"request": request}).data,
    }
