# Fabelio Challenge - Pradipta Gitaya
This is a submission for the challenge from Fabelio for Software Engineer Internship Program 2020

## Basic Information
This section contains some basic information regarding this application

Status for master branch:
[![pipeline status](https://gitlab.com/rd.pradipta/fabelio-challenge/badges/master/pipeline.svg)](https://gitlab.com/rd.pradipta/fabelio-challenge/commits/master) [![coverage report](https://gitlab.com/rd.pradipta/fabelio-challenge/badges/master/coverage.svg)](https://gitlab.com/rd.pradipta/fabelio-challenge/commits/master)

### Application Link
Frontend Service Link:
[Click Here for Frontend Service](https://dipsi-challenge.herokuapp.com/)

Backend Service Link:
[Click Here for Backed Service](https://dipsi-challenge-backend.herokuapp.com/__admin__/)

Documentation Link:
[Click Here for App Documentation](https://docs.google.com/document/d/1kTBpAxtXT8sOdYzWI5g9-iTelC3EvACZtUeV-WqeVPw/edit?usp=sharing)

### Installation Procedure (Development)
To install this project on your local computer, please follow instructions below. **Keep in mind that you have to run 2 windows/instances of terminal at the same time** (one instance running backend & one instance running frontend)

#### Backend Setup
- Make sure you have Python 3 installed. Please follow this link for installation of Python 3 [Python Download](https://www.python.org/downloads/)
- Install Pip (For example using APT)
```sudo apt install python3-pip```
- Install virtual environment on your device
```sudo pip3 install virtualenv```
- Go to backend folder and create a virtual environment
```python3 -m venv env```
- Activate your virtual environment
```source env/bin/activate```
- Install Django + other dependencies for backend
```pip3 install -r requirements.txt```
- Migrate Django ORM database (Option 1)
```
python3 manage.py makemigrations CHALLENGE
python3 manage.py migrate CHALLENGE
python3 manage.py migrate
```
- Migrate Django ORM database using shell script (Option 2)
```bash ./setup_dev.sh```
- Seed Initial Data for Database (No need if already ran the shell script)
```python3 manage.py loaddata initial_data.json```
- Run unit tests (Optional)
```python3 manage.py test```
- Activate Backend app
```python3 manage.py runserver```

#### Frontend Setup
- Make sure you have Node.js installed. Please follow this link for installation of NPM [Node.js Download](https://nodejs.org/en/download/)
- Go to frontend folder and install React.js + other dependencies for frontend
```npm install```
- Activate Frontend app
```npm start```

Note: Frontend service was developed by using Create React App (CRA) with theme from [Material Pro](https://www.wrappixel.com/templates/materialpro-react-admin-lite/)
