import Home from '../views/home.jsx';

var AppRoutes = [
  {
    path: '/home',
    name: 'Home Page',
    icon: 'mdi mdi-home',
    component: Home
  },
  {
    path: '/',
    pathTo: '/home',
    name: 'Home Page',
    redirect: true
  }
];

export default AppRoutes;
