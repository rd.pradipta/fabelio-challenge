import React from 'react';
import {
	Navbar,
	NavbarBrand,
} from 'reactstrap';

const Header = () => {

	return (
		<header className="topbar navbarbg" data-navbarbg="skin4">
			<Navbar className="top-navbar" dark expand="md">
				<div className="navbar-header" id="logobg" data-logobg="skin4">
					<NavbarBrand href="/" className="mr-auto">Product Showcase - Fabelio Challenge</NavbarBrand>
				</div>
			</Navbar>
		</header>
	);
}
export default Header;
