import React from 'react';
import { Button, Card, CardBody } from 'reactstrap';

const ProductCard = (props) => {
	return (
		<Card className="blog-widget" style={{ height: '300px' }}>
			<CardBody>
				<div className="blog-image">
					<img 
						src={props.image_link}
						style={{ height: '150px' }}
						className="img-fluid"
						alt="product-img"
					/>
				</div>
				<h3>{props.name}</h3>
				<p className="mt-3 mb-3">Rp. {props.price}</p>
				<Button onClick={props.onClick} color="danger">
					See Details
				</Button>				
			</CardBody>
		</Card>
	);
}

export default ProductCard;