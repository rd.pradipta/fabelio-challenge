import React from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter, Table } from 'reactstrap';

const ProductDetail = (props) => {
	return (
		<Modal isOpen={props.isOpen} toggle={props.onToggle}>
			<ModalHeader toggle={props.onToggle}>Product Detail</ModalHeader>
			<ModalBody>
				<div className="blog-image">
					<img 
						src={props.image_link}
						className="img-fluid"
						alt="product-img"
					/>
				</div>
				<br/>
				<h3 style={{textAlign: "center"}}>{props.name}</h3>
				<Table borderless>
					<tbody>
						<tr>
							<th>Price</th>
							<td>Rp. {props.price}</td>
						</tr>
						<tr>
							<th>Dimension</th>
							<td>{props.dimension}</td>
						</tr>
						<tr>
							<th>Availabe Colours</th>
							<td>{props.colour}</td>
						</tr>
						<tr>
							<th>Material</th>
							<td>{props.material}</td>
						</tr>
					</tbody>
				</Table>
			</ModalBody>
			<ModalFooter>
				<Button color="secondary" onClick={props.onToggle}>Close</Button>
			</ModalFooter>
		</Modal>
	);
}

export default ProductDetail;