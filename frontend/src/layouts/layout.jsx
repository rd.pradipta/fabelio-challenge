import React from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';
import Header from '../components/header/header.jsx';
import AppRoutes from '../routes/routing.jsx';

const Layout = () => {
	return (
		<div
			id="main-wrapper"
			data-theme="light"
			data-layout="vertical"
			data-header-position="fixed"
			data-boxed-layout="full"
		>
			<Header />
			<div className="page-wrapper d-block">
				<div className="page-content container-fluid">
					<Switch>
						{AppRoutes.map((prop, key) => {
							if (prop.redirect) {
								return (
									<Redirect from={prop.path} to={prop.pathTo} key={key} />
								);
							} else {
								return (
									<Route
										path={prop.path}
										component={prop.component}
										key={key}
									/>
								);
							}
						})}
					</Switch>
				</div>
			</div>
		</div>
	);
}

export default Layout;
