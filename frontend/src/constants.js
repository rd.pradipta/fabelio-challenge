/**
 *
 * global-constants.js
 *
 * This component contains all constants defined
 *
 */

const devConfig = {
  url: {
    API_URL: 'http://localhost:8000/v1/',
  },
};

const prodConfig = {
  url: {
    API_URL: 'https://dipsi-challenge-backend.herokuapp.com/v1/',
  },
};

// Environment Switcher
export const config =
  process.env.NODE_ENV === 'development' ? devConfig : prodConfig;
