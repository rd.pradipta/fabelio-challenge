/*
 * API
 *
 * This file contains reusable axios instance for components.
 *
 */

import axios from 'axios';
import { config } from './constants';

const BackendService = axios.create({ baseURL: config.url.API_URL });

function getProducts() {
  return BackendService.get('/products');
}

function getSimilarProduct(productId) {
  return BackendService.get(`/product/similar/${productId}`);
}

export default {
  getProducts,
  getSimilarProduct,
};
