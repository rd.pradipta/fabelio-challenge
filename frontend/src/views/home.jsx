import api from '../api';
import React, { Component } from 'react';
import { Col, Row } from 'reactstrap';
import ProductCard from '../components/product-card/product-card';
import ProductDetail from '../components/product-detail/product-detail';

class Home extends Component {
	constructor(props) {
		super(props);
		this.handleOpenModal = this.handleOpenModal.bind(this);
		this.handleModal = this.handleModal.bind(this);
		this.state = {
			products: [],
			modalOpen: false,
			productIdToSee: 1,
			recommendedProduct: {},
			lastSeenProductId: 0,
			isLoadingSimilarProduct: false,
		};
	}

  async fetchProducts() {
    return api
      .getProducts()
      .then(response => {
				this.setState({ products: response.data });
			});
  }

	async fetchSimilarProduct(productId) {
		return api
			.getSimilarProduct(productId)
			.then(response => {
				this.setState({
					recommendedProduct: response.data,
					isLoadingSimilarProduct: false,
				});
			});
	}

	handleOpenModal(productId) {
		this.setState({
			modalOpen: true,
			productIdToSee: productId,
		})
	}

	handleModal() {
    if (this.state.modalOpen) {
			this.setState({
				lastSeenProductId: this.state.productIdToSee,
				isLoadingSimilarProduct: true,
			});
			this.fetchSimilarProduct(this.state.productIdToSee);
    }		
    this.setState(prevState => ({
      modalOpen: !prevState.modalOpen,
    }));
	}

  componentDidMount() {
		this.fetchProducts();
		this.fetchSimilarProduct(0);
  }

	render() {
		let mainPage;
		
		if (this.state.products.length === 0) {
			mainPage = (
				<h3 style={{textAlign: "center"}}>Please wait, Application is fetching data from server...</h3>
			)
		} else {
      mainPage = (
				<>
					<h2 style={{textAlign: "center"}}>Similar Product You May Love</h2>
					{this.state.lastSeenProductId === 0 ? (
						<h5 style={{textAlign: "center"}}>Check Out Our Best Furnitures!</h5>
					) : (
						<h5 style={{textAlign: "center"}}>
							Because You Saw {this.state.products[this.state.lastSeenProductId - 1].name}
						</h5>
					)}
					<br/>
					{this.state.isLoadingSimilarProduct ? (
						<h4 style={{textAlign: "center"}}>Please wait. Getting similar product...</h4>
					):(
						<Row>
						<Col md="4" lg="3">
							<ProductCard
								name={this.state.recommendedProduct.name}
								price={this.state.recommendedProduct.price}
								image_link={this.state.recommendedProduct.image_link}
								onClick={() => this.handleOpenModal(this.state.recommendedProduct.id)}
							/>
						</Col>
					</Row>
					)}
					<hr/>
					<h2 style={{textAlign: "center"}}>All Availabe Products</h2>
					<br/>
					<Row>
					{
						this.state.products.map(product => (
							<Col md="4" lg="3" key={product.id}>
								<ProductCard
									name={product.name}
									price={product.price}
									image_link={product.image_link}
									onClick={() => this.handleOpenModal(product.id)}
								/>
							</Col>
						))
					}
					</Row>
					<ProductDetail
						isOpen={this.state.modalOpen}
						onToggle={this.handleModal}
						name={this.state.products[this.state.productIdToSee - 1].name}
						price={this.state.products[this.state.productIdToSee - 1].price}
						dimension={this.state.products[this.state.productIdToSee - 1].dimension}
						colour={this.state.products[this.state.productIdToSee - 1].colour}
						material={this.state.products[this.state.productIdToSee - 1].material}
						image_link={this.state.products[this.state.productIdToSee - 1].image_link}
					/>
				</>
      );
		};
		return <>{mainPage}</>;
	}
}

export default Home;